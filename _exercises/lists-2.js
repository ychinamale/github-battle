// https://codepen.io/tylermcginnis/pen/eXPVpQ?editors=0010

class List extends React.Component {
  render() {
    // Render a list using the "friends" being passed in.
    
    return (
      <ul>
        
      </ul>
    )
  }
}

ReactDOM.render(
  <List friends={[
    'Mikenzi',
    'Cash',
    'Steven',
    'Kimmy',
    'Doug'
  ]} />,
  document.getElementById('app')
);