const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: "./app/index.js",
  devServer: {
    contentBase: path.join(__dirname, "./dist/"),
    port: 3000
  },
  module: {
    rules: [
      { test: /\.svg$/, use: "svg-inline-loader" },
      { test: /\.(js)$/, 
        use: {
          loader: "babel-loader",
          options: {
            presets: [ "@babel/preset-env", "@babel/preset-react" ]
          }
        },
        exclude: /(node_modules)/
      },
      { test: /\.css$/, use: [ "style-loader", "css-loader" ] }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "index_bundle.js"
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./app/index.html"
    })
  ],
  mode: process.env.NODE_ENV === "production" ? "production" : "development"
}