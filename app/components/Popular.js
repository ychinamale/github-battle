import React from 'react'

export default class Popular extends React.Component {
  render() {
    const languages = ['All', 'CSS', 'JavaScript', 'Java', 'Python', 'Ruby']

    return (
      <ul className='flex-center'>
        {languages.map((language) => (
          <li key={language}>
            <button className='btn-clear nav-link'>
              {language}
            </button>
          </li>
        ))}
      </ul>
    )
  }
}