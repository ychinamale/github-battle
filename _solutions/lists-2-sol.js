class List extends React.Component {
  render() {
    // Render a list using the "friends" being passed in.    
    return (
      <ul>
        { this.props.friends.map( (friend) => (
          <li id={this.props.friends.indexOf(friend)} >
            {friend}
          </li>
        )) }
        
      </ul>
    )
  }
}

ReactDOM.render(
  <List friends={[
    'Mikenzi',
    'Cash',
    'Steven',
    'Kimmy',
    'Doug'
  ]} />,
  document.getElementById('app')
);